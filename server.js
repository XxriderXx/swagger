const express = require('express');
const bodyParser = require('body-parser')
const swaggerUi = require('swagger-ui-express')
const jsyaml = require('js-yaml')
const fs = require('fs')
const path = require('path')
const config = require('./config/config')

const app = express()

app.use(function(req, res, next) {
    res.header('Access-Control-Allow-Origin', '*')
    res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,PATCH,OPTIONS');
    res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Authorization, Accept')
    next()
})

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json());


// const router = express.Router()
// app.use('/inlaska',router)
// routes(router)

var spec = fs.readFileSync(path.join(__dirname, 'swagger.yaml'), 'utf8');
var swaggerDocument = jsyaml.safeLoad(spec);

app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.get('/', (req, res) => res.status(200).send("ping"))

app.use(function(err, req, res, next) {
    res.status(500);
    res.send("Oops, something went wrong.")
 });

app.listen(config.port,()=>{
    console.log('Server runnig on http://localhost:'+config.port)
})
